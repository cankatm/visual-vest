import React from 'react';
import { 
  StyleSheet, 
  Text, 
  View,
  FlatList,
  StatusBar,
  Dimensions,
  TouchableOpacity
} from 'react-native';

const WPW = 12;
const WPH = 8;
const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const SQUARE_SIZE = WINDOW_WIDTH / WPW;

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dummyData: [],
      colorHexData: ['a', 'b', 'c', 'd', 'e', 'f', 1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
    }
  }

  makeColorArray = () => {
    let colorArray = [];
    for (let index = 0; index < WPW * WPH; index++) {
      let newColor = '#' 
                    + this.state.colorHexData[Math.floor(Math.random() * this.state.colorHexData.length)]
                    + this.state.colorHexData[Math.floor(Math.random() * this.state.colorHexData.length)]
                    + this.state.colorHexData[Math.floor(Math.random() * this.state.colorHexData.length)]
                    + this.state.colorHexData[Math.floor(Math.random() * this.state.colorHexData.length)]
                    + this.state.colorHexData[Math.floor(Math.random() * this.state.colorHexData.length)]
                    + this.state.colorHexData[Math.floor(Math.random() * this.state.colorHexData.length)];
      colorArray.push(newColor);
    }
    
    this.setState({ dummyData: colorArray });
  }

  changeColorsInSecs = () => {
    setInterval(() => {
      this.makeColorArray();
    }, 1)
  }

  componentWillMount() {
    this.changeColorsInSecs();
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar hidden />
        <FlatList 
          data={this.state.dummyData}
          renderItem={({item}) =><View style={{ width: SQUARE_SIZE, height: SQUARE_SIZE, backgroundColor: item }} />}
          keyExtractor={(item, index) => item}
          numColumns={WPW}
        />

        <View style={{ width: WINDOW_WIDTH, alignItems: 'center', justifyContent: 'center'}} >
          <TouchableOpacity onPress={() => this.makeColorArray()} >
            <View style={{ width: 80, height: 80, borderRadius: 40, backgroundColor: 'orange', marginTop: 64}} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
